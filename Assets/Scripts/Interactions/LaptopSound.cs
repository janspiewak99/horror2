﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaptopSound : MonoBehaviour, IClick
{
    [SerializeField]
    private AudioSource _laptopSound;
    public void TakeClick()
    {
        _laptopSound.Play();
    }
}
