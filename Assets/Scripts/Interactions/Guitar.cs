﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guitar : MonoBehaviour, IClick
{
    [SerializeField]
    private AudioSource _guitar;
    public void TakeClick()
    {
        _guitar.Play();
    }
}
