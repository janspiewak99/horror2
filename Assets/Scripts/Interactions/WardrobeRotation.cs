﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class WardrobeRotation : MonoBehaviour, IClick
{
    [SerializeField]
    private GameObject _wardrobe;
    private bool _wardrobeOpened = false;
    private float _openingAmount = 90;
    private float _rotateOnStart;
    private float _wardrobeSpeed = 3;
    private Vector3 _wardrobeVector;
    [SerializeField]
    //private AudioSource _closingWardrobeSound;

    private void Start()
    {
        _rotateOnStart = _wardrobe.transform.rotation.eulerAngles.y;
        _wardrobeVector = new Vector3(0, 90, 0);
    }
    public void TakeClick()
    {

       // _openingDoorSound.Play();
        if (!_wardrobeOpened)
        {
            _wardrobe.transform.DORotate(new Vector3(0, _rotateOnStart + _openingAmount, 0), _wardrobeSpeed).OnComplete(() =>
            {
                _wardrobeOpened = true;

            })

            ;
        }
        else
        {
            _wardrobe.transform.DORotate(new Vector3(0, _rotateOnStart, 0), _wardrobeSpeed).OnComplete(() =>
            {
                _wardrobeOpened = false;
                //_closingWardrobeSound.Play();
            });
        }

    }
    
    
}
