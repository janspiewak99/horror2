﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : MonoBehaviour, IClick
{
    public GameObject NotePanel;
    private bool _activePanel = false;
    private bool _inRange = false;
    private int _moving = 1;
    private int _stop = 0;
    [SerializeField]
    private AudioSource _paperSound;
    [SerializeField]
    private bool _isTheLastOne = false;
    [SerializeField]
    private Animator _anim;
    public void TakeClick()
    {
            
            NotePanel.SetActive(true);
            _activePanel = true;
            _paperSound.Play();
            Time.timeScale = _stop;
    }
    private void Update()
    {
        if (_activePanel && Input.GetKeyDown(KeyCode.Q))
        {
            Time.timeScale = _moving;
            NotePanel.SetActive(false);
            _paperSound.Play();
            _activePanel = false;
            if (_isTheLastOne)
            {
                _anim.Play("FinalDark");
                StartCoroutine(FinalDark());
                
            }
        }
    }
    IEnumerator FinalDark()
    {
        yield return new WaitForSeconds(4);
        LevelManager.Instance.MainMenu();
    }
}
