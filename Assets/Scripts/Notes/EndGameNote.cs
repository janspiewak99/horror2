﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameNote : MonoBehaviour, IClick
{
    [SerializeField]
    private Animator _anim;
    public void TakeClick()
    {
        Debug.Log("Start");
        StartCoroutine(FinalDark());
    }
    IEnumerator FinalDark()
    {
        Debug.Log("Start");
        yield return new WaitForSeconds(2);
        Debug.Log("End");
        _anim.Play("FinalDark");
        LevelManager.Instance.FirstLevel();
    }
}
