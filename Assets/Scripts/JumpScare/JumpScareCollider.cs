﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScareCollider : MonoBehaviour
{
    public bool _elevatorKeyOwning =false;
    [SerializeField]
    private LimboManAnimController _limboMan;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovement>()&& _elevatorKeyOwning)
        {
            _limboMan.ScareSomeone();
        }
    }
}
