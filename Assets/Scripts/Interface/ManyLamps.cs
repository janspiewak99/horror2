﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManyLamps : MonoBehaviour, IClick
{
    [SerializeField]
    private ManyLampsMedium _medium;
    [SerializeField]
    private AudioSource _sound;


   
    public void TakeClick()
    {
        _medium.Clicking();
        //_light.SetActive(!_lightOn);
        //_lightOn = !_lightOn;
        _sound.Play();
    }
}
