﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour, IClick
{
    [SerializeField]
    private GameObject _light;
    [SerializeField]
    private AudioSource _sound;
    private bool _lightOn = true;
    

    private void Start()
    {
        if (_light.activeSelf)
            _lightOn = true;
        else
            _lightOn = false;
    }
    public void TakeClick()
    {
        _light.SetActive(!_lightOn);
        _lightOn = !_lightOn;
        _sound.Play();
    }
}
