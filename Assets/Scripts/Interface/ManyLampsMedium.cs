using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManyLampsMedium : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _light;
    [SerializeField]
    private bool _lightOn = true;
    private void Start()
    {
        foreach (GameObject i in _light)
        {
            i.SetActive(_lightOn);
        }
    }
    public void Clicking()
    {
        _lightOn = !_lightOn;
        foreach (GameObject j in _light)
        {
            
            j.SetActive(_lightOn);
        }
    }
}
