﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlightTaking : MonoBehaviour, IClick
{
    [SerializeField] private Flashlight _flashlight;
    public void TakeClick()
    {
        _flashlight.Take();
        Destroy(this.gameObject);
    }
}
