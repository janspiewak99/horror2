﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    [SerializeField] private GameObject _flashlight;
    private bool _flashlightReloaded = true;
    private float _reloadTime = 0.5f;
    private bool _flashlightOwning = false;
    [SerializeField] private AudioSource _flashClick;
    void Update()
    {
        if (Input.GetKey(KeyCode.T) && _flashlightReloaded && _flashlightOwning)
        {
            Click();
        }
    }
    IEnumerator FlashlightReloading()
    {
        _flashlight.SetActive(!_flashlight.activeSelf);
        yield return new WaitForSeconds(_reloadTime);
        _flashlightReloaded = true;
    }
    private void Click()
    {
        _flashlightReloaded = false;
         StartCoroutine(FlashlightReloading());
        _flashClick.Play();
    }
    public void Take()
    {
        _flashlightOwning = true;
        Click();
    }
}
