﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    private CharacterController _cc;
    [SerializeField] private AudioSource _footStepSound;
    void Start()
    {
        _cc = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (_cc.velocity.magnitude > 1 && !_footStepSound.isPlaying)
        {
            _footStepSound.volume = Random.Range(0.6f ,0.7f);
            _footStepSound.pitch = Random.Range(0.8f, 1.1f);
            _footStepSound.Play();
        }
    }
}
