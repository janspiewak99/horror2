﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearnRayView : MonoBehaviour
{
    private float _Range = 0.5f;
    private Camera _fpsCamera;

    private void Awake()
    {
        _fpsCamera = GetComponentInParent<Camera>();
    }

    private void Update()
    {
        Vector3 lineOrigin = _fpsCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        Debug.DrawRay(lineOrigin, _fpsCamera.transform.forward * _Range, Color.green);
    }
}
