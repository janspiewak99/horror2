using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearnRaycastShoot : MonoBehaviour
{
    [SerializeField] private float _weaponRange = 50f;
    [SerializeField] private Transform _rayEnd;
    
    private Camera _fpsCamera;
    private LineRenderer _laserLine;

    private Vector3 _rayCenter = new Vector3(0.5f, 0.5f, 0); 

    [SerializeField] private GameObject _interactionInformation;
    
    private void Awake()
    {
        _laserLine = GetComponent<LineRenderer>();
        _fpsCamera = GetComponentInParent<Camera>();
    }

    private void Update()
    {
        Vector3 rayOrigin = _fpsCamera.ViewportToWorldPoint(_rayCenter); //Interaction Direction (Center of the screen)
        RaycastHit hit;

        RayCheck();
    }

    private void RayCheck()
    {
        if (Physics.Raycast(rayOrigin, _fpsCamera.transform.forward, out hit, _weaponRange) && hit.collider.TryGetComponent<IClick>(out var obj))
            {
                _interactionInformation.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (obj != null)
                    {
                        obj.TakeClick(); //Interaction IClick
                    }          
                }
            }
        else
            {
                _interactionInformation.SetActive(false);
            }
    }
}
