﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorOpenClose : MonoBehaviour
{
    [SerializeField]
    private Animator _anim;
    private bool _isOpened = false;
    public void Click()
    {
        if (_isOpened)
        {
            _anim.Play("Closing");
          
        }
        else
        {
            _anim.Play("Opening");
        }
        _isOpened = !_isOpened;
    }
}
