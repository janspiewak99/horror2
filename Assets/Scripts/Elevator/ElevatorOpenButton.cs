﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorOpenButton : MonoBehaviour, IClick
{
    [SerializeField]
    private ElevatorOpenClose _elevatorDoors;
    [SerializeField]
    private AudioSource _beepOpenClose;

    public void TakeClick()
    {
        _beepOpenClose.Play();
        _elevatorDoors.Click();
    }
}
