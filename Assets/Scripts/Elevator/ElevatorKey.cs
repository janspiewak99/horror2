﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorKey : MonoBehaviour, IClick
{
    [SerializeField] private TeleportationClick _keyToElevator;
    [SerializeField] private JumpScareCollider _colliderOfLimboMan;
    private bool _elevatorKey = false;
    [SerializeField]
    private AudioSource _soundOfKeys;

    public void TakeClick()
    {
        _colliderOfLimboMan._elevatorKeyOwning = true;
        _keyToElevator._elevatorKeyOwning = true;
        //_soundOfKeys.Play();
        Destroy(this.gameObject);

    }
}
