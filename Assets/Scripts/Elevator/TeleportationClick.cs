﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportationClick : MonoBehaviour, IClick
{
    [SerializeField]
    private AudioSource _noKeyDetected;
    [SerializeField]
    private AudioSource _elevatorGoesDownSound;
    [SerializeField]
    private AudioSource _elevatorBeepAscending;
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private Transform _teleportPoint;
    //[HideInInspector]
    public bool _elevatorKeyOwning = true;
    [SerializeField]
    private Animator _anim;
    [SerializeField]
    private GameObject _floor1;
    [SerializeField]
    private GameObject _floor2;
    public void TakeClick()
    {
        if (_elevatorKeyOwning)
        {
            _elevatorBeepAscending.Play();
            _anim.Play("Darkening");
            _elevatorGoesDownSound.Play();
            StartCoroutine(TimeBetweenFloors());
        }
        else
        {
            _noKeyDetected.Play();
        }
        
    }
    IEnumerator TimeBetweenFloors()
    {
        yield return new WaitForSeconds(2) ;
        _floor2.SetActive(true);
        _player.transform.position = _teleportPoint.transform.position;
        //_floor1.SetActive(false); // na przyszłość nie usuwać 
    }
}
