﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorRotate : MonoBehaviour
{
    [SerializeField]
    private GameObject _doors;
    private bool _doorOpened = false;
    [SerializeField] private float _openingAmount = -90;
    private float _rotateOnStart;
    private float _doorSpeed = 3;
    //private Vector3 door;
    [SerializeField]
    private AudioSource _closingDoorSound;
    [SerializeField]
    private AudioSource _openingDoorSound;

    private void Start()
    {
        _rotateOnStart = _doors.transform.rotation.eulerAngles.y;
       // door = new Vector3(0, _openingAmount, 0);
    }
    public void Interact()
    {

        _openingDoorSound.Play();
        if (!_doorOpened)
        {
            _doors.transform.DORotate(new Vector3(0, _rotateOnStart + _openingAmount, 0), _doorSpeed).OnComplete(() =>
            {
                _doorOpened = true;

            });

            
        }
        else
        {
            _doors.transform.DORotate(new Vector3(0, _rotateOnStart, 0), _doorSpeed).OnComplete(() =>
            {
                _doorOpened = false;
                _closingDoorSound.Play();
            });
        }

    }


}