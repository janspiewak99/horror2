﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTake : MonoBehaviour, IClick
{
    [SerializeField]private DoorKeyOwning _keyToDoors;
    [SerializeField]
    private AudioSource _soundOfKeys;
    public void TakeClick()
    {
     
            _keyToDoors.keyOwning = true;
            _soundOfKeys.Play();
            Destroy(this.gameObject);
        
    }
}
