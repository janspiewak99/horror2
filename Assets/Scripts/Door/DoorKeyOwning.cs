﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKeyOwning : MonoBehaviour, IClick
{
    [SerializeField]
    private DoorRotate Rotate;
    public bool keyOwning = false;
    //private bool _doorDistance = false;
    [SerializeField]
    private AudioSource _closedDorrSoudn;
    public void TakeClick()
    {
        if (keyOwning)
        {
            Rotate.Interact();
        }
        else
        {
            _closedDorrSoudn.Play();
        }
    }
}
